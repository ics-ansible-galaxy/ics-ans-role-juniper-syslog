# ics-ans-role-juniper-syslog

Ansible role to configure juniper switches to send syslog messages to graylog.

See documentation for what it can log and which severity level
https://www.juniper.net/documentation/en_US/junos/topics/reference/general/syslog-facilities-severity-levels.html
## Role Variables

```yaml
---
juniper_syslog_remote_host: 172.16.107.59
juniper_syslog_remote_host_port: 514
juniper_syslog_facilities: []
  # - facility: any
  #   level: emergency
  # - facility: authorization
  #   level: info
  # - facility: interactive-commands
  #   level: any
  # - facility: security
  #   level: warning

```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-juniper-syslog
```

## License

BSD 2-clause
